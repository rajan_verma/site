// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or any plugin's vendor/assets/javascripts directory can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file.
//
// Read Sprockets README (https://github.com/rails/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require jquery
//= require jquery_ujs
//= require turbolinks
//= require bootstrap
//= require js/jquery.flexslider/jquery.flexslider.js
//= require js/html5shiv
//= require js/jquery.appear.js
//= require js/jquery.countTo.js
//= require js/jquery.nicescroll.min.js
//= require js/jquery.parallax-1.1.3.js
//= require js/jquery.videoBG.js
//= require js/respond.min.js
//= require js/responsiveslides.min.js
//= require waypoint/waypoints.min.js
//= require slide.js
//= require onepage.js
//= require js.js