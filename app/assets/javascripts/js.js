
(function($) {
/**
   * Copyright 2012, Digital Fusion
   * Licensed under the MIT license.
   * http://teamdf.com/jquery-plugins/license/
   *
   * @author Sam Sehnert
   * @desc A small plugin that checks whether elements are within
   *     the user visible viewport of a web browser.
   *     only accounts for vertical position, not horizontal.
   */
$.fn.visible = function(partial) {
    
      var $t            = $(this),
          $w            = $(window),
          viewTop       = $w.scrollTop(),
          viewBottom    = viewTop + $w.height(),
          _top          = $t.offset().top,
          _bottom       = _top + $t.height(),
          compareTop    = partial === true ? _bottom : _top,
          compareBottom = partial === true ? _top : _bottom;
    
    return ((compareBottom <= viewBottom) && (compareTop >= viewTop));

  };

  $.fn.scrollView = function () {
  return this.each(function () {
    $('html, body').animate({
      scrollTop: $(this).offset().top
    }, 1000);
  });
};

$.fn.getNextFolio = function ($loadNew,$callingParent,$slider){
								$sliderData=$('.folio-slider-wrapper').data('flexslider')
								if($loadNew==true)
			  	 				{

			  	 					//load new slide if that category is not already loaded	
			  	 					$folio_cat = jQuery($callingParent).data("cat");
							      $folio_page = jQuery($callingParent).data("page");
							      $data="action=nexPrevPortfolio&folio-cat="+$folio_cat+"&folio-page="+$folio_page;
							      jQuery.ajax({
							         type : "post",
							         dataType : 'JSON',
							         url : myAjax.ajaxurl,
							         data : {
							         			'action': 'nexPrevPortfolio',
							         			'folio-cat': $folio_cat,
							         			'folio-page' : $folio_page
							         		},
							         success: function(response) {
							         	$sliderWrapper=jQuery(".folio-slider-enclose");
										$slideCount=$sliderWrapper.data('slide-count');
							            $sliderWrapper.append(_.unescape(response.post));
							            $newSlide=$sliderWrapper.find('.folio-slide:last-of-type');
							           
							            $sliderData.addSlide($newSlide);
							            $sliderData.flexAnimate($sliderData.getTarget("next"), true);
							            $slideCount = $sliderData.currentSlide;
							           	$callingParent.data('slide',$slideCount);
							           	$sliderWrapper.data('slide-count',$slideCount);
							           	$activeSlide=$('.active-slide');
							           	$activeSlide.data("cat",$folio_cat);
							           	$callingParent.data("max-page",response.maxPage);
							           	$callingParent.data("max-page");
							           	$callingParent.data("page",1);
							           	$callingParent.showPortNav($callingParent);
							         }
							      })
			  	 				}
			  	 				else{
			  	 					//take to already loaded slide
			  	 					$gSlide=$callingParent.data('slide');
			  	 					$slider.flexslider($gSlide);

			  	 				}
			  	 				
		return this;

};

$.fn.getNextFolioPage = function ($appendToSlide,$callingParent,$slider){

								
			  	 				  $folio_cat = jQuery($callingParent).data("cat");
							      $folio_page = jQuery($callingParent).data("page");
							      $data="action=nexPrevPortfolio&folio-cat="+$folio_cat+"&folio-page="+$folio_page;
							      ////console.log($data);
							      jQuery.ajax({
							         type : "post",
							         dataType : 'JSON',
							         url : myAjax.ajaxurl,
							         data : {
							         			'action': 'nexPrevPortfolio',
							         			'folio-cat': $folio_cat,
							         			'folio-page' : $folio_page
							         		},
							         success: function(response) {
							         	$appendToSlide.empty();
							         	
							         	response=$appendToSlide.append($(_.unescape(response.post)).html());
							         	//$currentSlide=$slider.getCurrentSlide;
							         	//$sliderObject.startSlide=$currentSlide;
							         	//$slider.reloadSlider($sliderObject);
							       	}
							      })
			  	 				
			  	 				
		return this;

};

$.fn.showPortNav=function ($callingParent){
				$this=$callingParent;
				$cat=$this.data("cat");
				$maxPage=$this.data("max-page");
				$page=$this.data("page");
				$next=jQuery('.next-port-page');
				$prev=jQuery('.prev-port-page');
				$next.data("page",$page+1);
				$prev.data("page",$page-1);
				$next.data("cat",$cat);
				$prev.data("cat",$cat);
				$next.css({
					visibility:'hidden'
				});
				$prev.css({
					visibility:'hidden'
				});
				if($page<$maxPage && $page!=0){
					$next.css({
					visibility:'visible'
				});
				}
				if($page>1){
					$prev.css({
					visibility:'visible'
				});
				}
		return this;
};

$.fn.getFolioItem = function ($callingParent){
								//load a overloy when you click on the portfolio item
								$folio_post = jQuery($callingParent).attr('data-id');
							     jQuery.ajax({
							         type : "post",
							         url : myAjax.ajaxurl,
							         data : {
							         			'action': 'getPortfolioItem',
							         			'folio-post': $folio_post,
							         		},
							         dataType: 'JSON',
							         success: function(response) {
							         	$('.single-folio-content-wrapper h2').append(response.post.post_title);
							         		
							         		if(response.port_desc==''&&response.post.post_content==''){
							         			$('#folio-single-auto').append('<img src="'+response.port_image+'"</img>');
							         			$('#single-folio-wrapper2').fadeIn(400);
							         			return;
							         		}
							         		$("#single-folio-wrapper").css({
							         			textAlign:'left'
							         		});
							         		if(response.port_desc==''){
							         			$('#folio-desc-content').hide();
							         			$('#single-folio-image').addClass('col-md-offset-3');
							         			$('#single-folio-content-wrapper').addClass('col-md-offset-3');

							         		}else{
							         			$('#folio-desc-content').append(_.unescape(response.port_desc));
							         		}
							         		if(response.post.post_content==''){
							         				$('#folio-content').hide();
							         				$('.single-folio-wrapper').css({
							         					width:'auto!important',
							         					margin:'auto!important'
							         				});

							         				$('#single-folio-content-wrapper').find('h2').hide();
							         		}
							         		else{
							         			$('#folio-content').append(_.unescape(response.post.post_content));
							         		}
							         		if(!(response.port_image=='')){

							         			$('#single-folio-image').append('<img src="'+response.port_image+'"</img>');
							         		}
							         			$('.single-folio-wrapper').fadeIn(400);
							         }
							      });
		return this;
};

    
})(jQuery);


(function($){
var $topLogo=$('.top-logo');
			var $topAwe=$('.top-awe');


		$('.top-logo').appear(function(){
					$(this).css({visibility:'visible'}).addClass('animated bounceInDown');
		});

		if($(window).height()<=480){
				$topLogo.css({
					maxWidth: '150px'
				});

				$topAwe.css({
					visibility:'hidden'
				});

				$('.grace-spinner-loader').css({
					bottom:'0',
					marginBottom:'10px'
				});
			}
			else{
				$topLogo.css({
					maxWidth: '100%'
				});

				$topAwe.css({
					visibility:'visible'
				})
			}
		
		

})(jQuery);




//function declaration ends
(function($){
jQuery(window).load(function(){
			//create the portfolio slider and other required functions for it
			slider=$('.folio-slider-wrapper').flexslider({
						namespace: "grace-", 
						selector: ".folio-slider-enclose > .folio-slide", 
						animation: "slide", 
						easing: "ease-out", 
						direction: "horizontal", 
						reverse: false, 
						animationLoop: false,
						smoothHeight: false, 
						startAt: 0, 
						slideshow: false, 
						animationSpeed: 1000, 
						controlNav: false, directionNav: false,
						initDelay: 0,
						touch: false,
						useCSS: true,
						before: function($slider){
							
							$slideElement=$('.folio-slide').not('.active-slide');

							$slideElement.find('.folio-tile,.folio-next').css({
					  							visibility:'hidden'
					  						});

		  					$slideElement.find('.folio-tile').removeClass('animate flipInX');
							jQuery('.slide-ajax').show();
							jQuery('.folio-slide').removeClass('active-slide');

						},
						start:function(){
							//set height of the not impress button
							var $h = $('.folio-next').parent().prev().find('figure').height();
							 $('.folio-next').css({
												height:$h
									});
							jQuery.waypoints('refresh');
						},
						after: function($slider){
								$slideElement=$('.folio-slide:nth-of-type('+($slider.currentSlide+1)+')');
								if(!$slideElement.hasClass('active-slide')){
									$slideElement.find('.folio-tile').each(function(i){
								  		var row = jQuery(this);
										 setTimeout(function() {
										    row.css({visibility:'visible'}).addClass('animated flipInX');
										 }, 80*(i));
									});
								}
								
								$page=$slideElement.data("page");
								$slideElement.addClass('active-slide');
					  			jQuery('.slide-ajax').fadeOut(400);
					  			jQuery.waypoints('refresh');
						} 
				});
				//listen for click on not impressed buton
				$('.folio-next-a .folio-tile').on('click',function(e){
				 	e.preventDefault();
				 	$('#portfolio header h1').hide(0);
				 	$('#portfolio header .port-nav').fadeIn();
					slider.flexslider("next");
					$callingParent=$('#all-folio');
					$callingParent.parent().addClass('active');
					
					$callingParent.showPortNav($callingParent);
				});
			//listen for click on category nav of portfolio slider
			jQuery(".port-cat-bit").click( function(e) {
				e.preventDefault();
				$this=$(this);
				$loadNew=$this.data('new-load');
				//////console.log($loadNew);
				$this.showPortNav($this);
				$this.getNextFolio($loadNew,$this,slider);
				$this.showPortNav($this);
		       	$this.data('new-load',false);
		     });
			
			Tslider=$('.testimonial-slider').flexslider({
						namespace: "grace-testimonial-", 
						selector: ".testimonial-slider-enclose > .tslide-wrapper", 
						animation: "fade", 
						direction: "horizontal",
						reverse: false,
						animationLoop: true,
						smoothHeight: true,
						startAt: 0, 
						slideshow: true, 
						slideshowSpeed: 5000, 
						animationSpeed: 800, 
						controlNav: false, 
						directionNav: false,
						initDelay: 0,
						useCSS: true,
						touch:false,
						before: function($slider){
										$newImg=$('.tslide-img[data-sid="'+$slider.getTarget("next")+'"]').html();
				  						$centerImg=$('.center-tslide').fadeOut(500,function(){
				  							$(this).empty().append($newImg).fadeIn()
				  						});
				  		}, 
				});
			//refresh waypoint	
			jQuery.waypoints('refresh');
			//listen for click on top arrow
			$('.top-awe').css({
				display:'block',
				visibility:'hidden',
			});
			//loader finishing animation
			$('.grace-spinner-loader').addClass("animated bounceOutDown");
				$('.top-awe').appear(function(){
					if($(window).height()>480){
						$(this).css({visibility:'visible'}).addClass("animated bounceInUp");
					}
					$('.big-header').css({
						position:'relative'
					});
					$('.home-bx-slider-wrapper-container').css({
						visibility:'visible'
					});
				})
			//set correct height of the slider not impressed
			var $h = $('.folio-next').parent().prev().find('figure').height();
			 $('.folio-next').css({
								height:$h
					});
			var $loadNew=false;
			var $callingParent=null;
			var $slideCount=$('.folio-slider-wrapper').data('slide-count');
			var $window=$(window);
			var $Bbg=$('.big-header');
			$Bbg=($Bbg.height());
			var $navTop=$('.top-nav');
			var $pfolio=$('.portfolio');
			var $topLogo=$('.top-logo');
			var $topAwe=$('.top-awe');
			$pnav=$('.port-nav-cat li');
			$pnav.on('click',function(){
					$pnav.removeClass('active');
					$(this).addClass('active');

			});
			
			$("#folio-enclose-wrapper").niceScroll();
			//initialize the portfolio slider
			

			 jQuery(window).resize(function() {
 					$h = $('.folio-next').parent().prev().find('figure').height();
    				
    				$('.folio-next').css({
						height:$h
					});
    				 
    				 if($(window).height()<=480){
							$topLogo.css({
								maxWidth: '150px'
							});

							$topAwe.css({
								visibility:'hidden'
							});

							$('.grace-spinner-loader').css({
								bottom:'0'
							});
						}
						else{
							$topLogo.css({
								maxWidth: '100%'
							});

							$topAwe.css({
								visibility:'visible'
							})
						}
						jQuery.waypoints('refresh');
    				
		    	});
			//listen for click on next and prev button of portfolio
			$('.port-page').on('click',function(e){
					e.preventDefault();
					$this=$(this);
					$enabled=$this.data('enabled');
					$Cat=$this.data("cat");
					$page=$this.data("page");
					if($enabled==true){
						if($this.hasClass('next-port-page'))
						{
								
								$appendToSlide=$('.folio-slide.active-slide');
								$this.getNextFolioPage($appendToSlide,$this,slider);
								$currentCat=$(".port-nav-cat li.active .port-cat-bit").data("page",$page);
								$currentCat.showPortNav($currentCat);
								
						}
						else{
								
								$appendToSlide=$('.folio-slide.active-slide');
								$this.getNextFolioPage($appendToSlide,$this,slider);
								$currentCat=$(".port-nav-cat li.active .port-cat-bit").data("page",$page);
								$currentCat.showPortNav($currentCat);
								
						}
					}
					
				});
		//ajax portfolio call
			
			$folio=$('#portfolio');
			var $animation=$('.animation');

			$window.scroll(function(event) {
			  if($window.width()>768){
					 $animation = $(".animation").each(function(i, el) {
					    var el = $(el);
					    if (el.visible(true)) {
					      animation=el.data('animate');
					      el.addClass(animation); 
					      el.removeClass('animation');
					    } 
					  });
			 }
			 	var $btm=$pfolio.offset().top+$pfolio.height();
			 	if($window.scrollTop() < $Bbg || ($pfolio.offset().top -100 < $window.scrollTop() && $window.scrollTop() < $btm-10)){
			
					$navTop.slideUp();
				}
				else {
					$navTop.addClass('fixed-nav').slideDown();
				}
			 
			 
			});
			

			  $(document).on('click','.folio-tile-a',function(e){
			 	e.preventDefault();
				if($(this).hasClass('folio-next-a')){
					return;
				}
				$('#folio-enclose-wrapper').slideDown(800);
				$(this).getFolioItem($(this));

			 	
			 });

			  $('#feat-folio,#all-folio').on('click',function(e){
			 		e.preventDefault();
			 		$this=$(this);
			 		$slide=$this.data('slide');
					////console.log($slide);
					slider.flexslider($slide);
					$callingParent=$(this);
					$callingParent.showPortNav($callingParent);
					$this.data('new-load',false);

			 });


	   $('.folio-single-close,.folio-overlay').on('click',function(e){
	 	e.preventDefault();
		
		$('#folio-enclose-wrapper').slideUp(800,function(){
			$("#single-folio-wrapper").hide();
			$("#single-folio-wrapper2").hide();
		$('#single-folio-wrapper2 #folio-single-auto').empty();
			$('.single-folio-content-wrapper h2').empty();
		$('#folio-desc-content').empty();
 		$('#folio-content').empty();
 		$('#single-folio-image').empty();
 		$('#folio-desc-content').show();
		$('#single-folio-image').removeClass('col-md-offset-3');
		$('#single-folio-content-wrapper').removeClass('col-md-offset-3');
		$('#folio-content').show();
			$('#single-folio-content-wrapper').find('h2').show();
					         		

		});
	 	
 	});

	$('.counterwrapper').appear(function() {
		var currentcounter=$(this).find('.counter');	
		
		c_element = currentcounter.html();
		
				currentcounter.countTo({
					from: 0,
					to: c_element,
					speed: 2500,
					refreshInterval: 50,
				});

	});

	$('#next-slide').on('click',function(){
		$('.non-parllax').first().scrollView();
	});

	$slide=$('article.onepage');		
	
	$slide.waypoint(function (event){
		//chache the variable of data-slide
		dataslide=$(this).attr('data-slide');
		if (event === 'down') {
			$('nav li a').removeClass('active');         
			 $('nav li a[data-id="' + dataslide + '"]').addClass('active').parent('li').prev('li a.active').removeClass('active');
        		
        }else {
        	 $('nav li a').removeClass('active');
            $('nav li a[data-id="' + dataslide + '"]').addClass('active').parent('li').next('li a.active').removeClass('active'); 
        }
	
	},{offset:'150px'});

	//RESPONSIVE HEADER SLIDER CODE
	$("#home-bx-slider-wrapper").responsiveSlides({
	  auto: true,             // Boolean: Animate automatically, true or false
	  speed: 1000,            // Integer: Speed of the transition, in milliseconds
	  timeout: 4000,          // Integer: Time between slide transitions, in milliseconds
	  pager: false,           // Boolean: Show pager, true or false
	  nav: false,             // Boolean: Show navigation, true or false
	  random: false,          // Boolean: Randomize the order of the slides, true or false
	  pause: false,           // Boolean: Pause on hover, true or false
	  pauseControls: true,    // Boolean: Pause when hovering controls, true or false

	});

	$('.page-template-Page-OnePage-php #gnav li a').on('click',function(e){
		e.preventDefault();
		$id=$(this).data('id');
		////console.log('onepage-'+$id);
		$op=$('.onepage-'+$id).first().scrollView();
		if($window.width()<768){
			$('.navbar-toggle').addClass('collapsed');
			$('.navbar-collapse').removeClass('in');

		}
	});
});
})(jQuery);