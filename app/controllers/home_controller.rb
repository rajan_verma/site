class HomeController < ApplicationController
	layout "homelayout"
	def index
		@first_slide = Portfolio.all.limit(10)
		@second_slide = Portfolio.all.offset(10).limit(12)
		@blog =  Blog.all.limit(3)
		@testimonial = Testimonial.all.limit(5)
		@fb_link = SiteConfig.fb_link
		@twitter_link = SiteConfig.twitter_link
		@gplus_link = SiteConfig.gplus_link
		@linked_link = SiteConfig.linked_link
		@flicker_link = SiteConfig.flicker_link
		@text_hex = SiteConfig.text_hex
		@number = SiteConfig.number
		@s1  = SiteConfig.s1
		@s2  = SiteConfig.s2
		@s3  = SiteConfig.s3
		@s4  = SiteConfig.s4
		@s5  = SiteConfig.s5
		@s6  = SiteConfig.s6
		@teams =  Team.all
	end
end