class DashboardController < ApplicationController
	layout "adminlayout"
	before_action :authenticate_user!

	def index
		
	end

	def show
		# @ins =  Portfolio.new
		# @ins.show
		 # render 'portfolio'
	end

	def portfolio
		@portfolios = Portfolio.all
	end

	def blog
		@blogs = Blog.all
	end

	def service
		@services = Service.all
	end

	def testimonial
		@testimonials = Testimonial.all
	end

	def settings

		@fb_link = SiteConfig.fb_link
		@twitter_link = SiteConfig.twitter_link
		@gplus_link = SiteConfig.gplus_link
		@linked_link = SiteConfig.linked_link
		@flicker_link = SiteConfig.flicker_link
		@text_hex = SiteConfig.text_hex
		@number = SiteConfig.number
		@s1  = SiteConfig.s1
		@s2  = SiteConfig.s2
		@s3  = SiteConfig.s3
		@s4  = SiteConfig.s4
		@s5  = SiteConfig.s5
		@s6  = SiteConfig.s6

	end

	def save_settings
		
		SiteConfig.fb_link = params[:fb_link] 
		SiteConfig.twitter_link = params[:twitter_link] 
		SiteConfig.gplus_link = params[:gplus_link] 
		SiteConfig.linked_link = params[:linked_link] 
		SiteConfig.flicker_link = params[:flicker_link] 
		#SiteConfig.img = params[:img]
		SiteConfig.number  = params[:number]
		SiteConfig.s1 = params[:s1]
		SiteConfig.s2 = params[:s2]
		SiteConfig.s3 = params[:s3]
		SiteConfig.s4 = params[:s4]
		SiteConfig.s5 = params[:s5]
		SiteConfig.s6 = params[:s6]
		redirect_to :back, notice: "Data Save!"
	end

	def team
		@teams = Team.all
	end
end
