class Blog < ActiveRecord::Base
	has_many :attachments
	accepts_nested_attributes_for :attachments
	#mount_uploader :image, ImageUploader
end
