json.array!(@blogs) do |blog|
  json.extract! blog, :id, :title, :image, :desc
  json.url blog_url(blog, format: :json)
end
