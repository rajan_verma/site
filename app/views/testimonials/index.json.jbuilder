json.array!(@testimonials) do |testimonial|
  json.extract! testimonial, :id, :profile_pic, :client_name, :company_name, :comment, :star
  json.url testimonial_url(testimonial, format: :json)
end
