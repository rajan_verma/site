json.array!(@services) do |service|
  json.extract! service, :id, :title, :image, :description
  json.url service_url(service, format: :json)
end
