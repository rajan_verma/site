json.array!(@portfolios) do |portfolio|
  json.extract! portfolio, :id, :title, :featured_image, :description
  json.url portfolio_url(portfolio, format: :json)
end
