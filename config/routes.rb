Rails.application.routes.draw do
  resources :teams
  resources :attachments
  resources :testimonials
  resources :services
  resources :blogs
  devise_for :users
  #devise_for :installs
  resources :portfolios
  # resources :dashboard
  # The priority is based upon order of creation: first created -> highest priority.
  # See how all your routes lay out with "rake routes".

  # You can have the root of your site routed with "root"
  # root 'welcome#index'

  # Example of regular route:
     root 'home#index'
     #get '/blog' => 'application#index'
     #get '/blog/:id' => 'blog#show'
     get '/dashboard' => 'dashboard#index'
     get '/dashboard/portfolio' => 'dashboard#portfolio'
     get '/dashboard/team' => 'dashboard#team'
     # get '/dashboard/blogs' => 'dashboard#blogs'
     # get '/dashboard/service' => 'dashboard#service'
     # get '/dashboard/testimonials' => 'dashboard#testimonials'
     get '/dashboard/settings' => 'dashboard#settings'
     post '/dashboard/settings' => 'dashboard#save_settings'

  # Example of named route that can be invoked with purchase_url(id: product.id)
  #   get 'products/:id/purchase' => 'catalog#purchase', as: :purchase

  # Example resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Example resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Example resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Example resource route with more complex sub-resources:
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', on: :collection
  #     end
  #   end

  # Example resource route with concerns:
  #   concern :toggleable do
  #     post 'toggle'
  #   end
  #   resources :posts, concerns: :toggleable
  #   resources :photos, concerns: :toggleable

  # Example resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end
end
