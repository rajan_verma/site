class CreateTeams < ActiveRecord::Migration
  def change
    create_table :teams do |t|
      t.string :name
      t.text :profile
      t.string :post
      t.text :detail

      t.timestamps null: false
    end
  end
end
