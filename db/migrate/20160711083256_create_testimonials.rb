class CreateTestimonials < ActiveRecord::Migration
  def change
    create_table :testimonials do |t|
      t.text :profile_pic
      t.string :client_name
      t.string :company_name
      t.text :comment
      t.integer :star

      t.timestamps null: false
    end
  end
end
