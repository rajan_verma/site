class CreatePortfolios < ActiveRecord::Migration
  def change
    create_table :portfolios do |t|
      t.string :title
      t.text :featured_image
      t.text :description

      t.timestamps null: false
    end
  end
end
