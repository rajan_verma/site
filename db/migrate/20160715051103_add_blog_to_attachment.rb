class AddBlogToAttachment < ActiveRecord::Migration
  def change
    add_reference :attachments, :blog, index: true, foreign_key: true
  end
end
